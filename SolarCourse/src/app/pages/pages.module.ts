import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { SharedModule } from '../shared/shared.module';

import { AppRoutingModule } from '../app-routing.module';
import {SkeletonModule} from 'primeng/skeleton';
import { NewAdvertisementPageComponent } from './newAdvertisement-page/newAdvertisement-page.component';
import { ComponentsModule } from '../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    SkeletonModule,
    ComponentsModule
    
  ],
  declarations: [
    MainPageComponent,
    NewAdvertisementPageComponent
  ],
  exports: [
    MainPageComponent,
    NewAdvertisementPageComponent
  ]
})
export class PagesModule { }
