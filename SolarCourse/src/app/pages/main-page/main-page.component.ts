import { Component, OnInit } from '@angular/core';
import { IAdvertisement } from 'src/app/interface/advertisement';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  isLoading: boolean = true;
  advertisements!: IAdvertisement [] ;

  constructor() {}

  ngOnInit( ) {
    setTimeout(() =>{
    this.advertisements =  new Array(20).fill({
      id: 0,
      title: 'Гитара фендер',
      price: 20000,
      imgSrc: 'https://placehold.co/600x400',
      createdAt: 'Сегодня 14:12',
      address: 'Москва,Ленинский проспект',
    }) 
    this.isLoading=false
    }, 2000);


  }

}
