import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


export enum formTypes {
  SignUp = "Регистрация",
  SignIn = "Авторизация",
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  
  
})
export class AuthComponent implements OnInit {
  SignUp: FormGroup
  SignIn: FormGroup 

  formTypes = formTypes;
  formType = this.formTypes.SignIn;

 
  constructor() {
    this.SignUp = new FormGroup({
      phone: new FormControl(null,[Validators.required,Validators.minLength(2)]),
      name: new FormControl(null,[Validators.required,Validators.minLength(10)]),
      password: new FormControl(null,[Validators.required,Validators.minLength(1)])

    })
    this.SignIn = new FormGroup({
      phone: new FormControl(null,[Validators.required,Validators.minLength(2)]),
      password: new FormControl(null,[Validators.required,Validators.minLength(1)])
     

    })
   }
  submit(){
    console.log(this.SignIn.value)
    if (this.SignIn.value.phone == "12" && this.SignIn.value.password == "12") {
      this.loginEvent.emit(true);
    }
    else this.loginEvent.emit(false);
  }
 


  @Output() loginEvent = new EventEmitter<boolean>();
  @Input() display: boolean = true;
  
  ngOnInit() {
    

    
  }

}
