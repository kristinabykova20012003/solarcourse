import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AuthComponent } from '../auth/auth.component';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  
})
export class MenuComponent implements OnInit {
  display:boolean = false;
  isLogin: boolean = false;

  items!: MenuItem[];
   
  showDialog() {
    this.display = true;
  }

  successLogin(allowedLogin: boolean){
    if (allowedLogin){
      this.display = false;
      this.isLogin = allowedLogin
    }  
  }
  constructor() {
   }


  ngOnInit() {
    this.items = [
      {label: 'Мои объявления',routerLink: ''},
      {label: 'Настройки',routerLink: ''},
      {label: 'Чаты',routerLink: ''},
      {
        label: 'Выйти',
        command:() =>{
          this.isLogin=false

        }
      }
  ];
  
  }

}
