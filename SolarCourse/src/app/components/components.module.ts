import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from '../app-routing.module';
import { SharedModule } from 'primeng/api';
import { MenuComponent } from './menu/menu.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuModule } from 'primeng/menu';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthComponent } from './auth/auth.component';

import {DynamicDialogModule} from 'primeng/dynamicdialog';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    DialogModule,
    ButtonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MenuModule,
    DynamicDialogModule

    
   

    
  ],
  declarations: [
    MenuComponent,
    NavbarComponent,
    AuthComponent
    
  ],
  exports: [
    MenuComponent,
    NavbarComponent,
    AuthComponent
  ]

})
export class ComponentsModule { }
