export interface IAdvertisement {
    id: number,
    title: string,
    price: number,
    imgSrc:string,
    createdAt: string,
    address:string  
}