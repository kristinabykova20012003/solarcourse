import { Component, Input, OnInit } from '@angular/core';
import { IAdvertisement } from 'src/app/interface/advertisement';

@Component({
  selector: 'app-product[advertisement]',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  constructor() {}

  @Input() advertisement!: IAdvertisement;
  isVisible = false;

  ngOnInit() {}
}
